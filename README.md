# Instructions

1. Clone the repository:

	$ git clone git@bitbucket.org:rafaelbarcellos/triangle.git
	
	or
	
	$ git clone	https://rafaelbarcellos@bitbucket.org/rafaelbarcellos/triangle.git

2. Enter the folder of the application:

		$ cd triangle

3. Run Bundle

		$ bundle install
	
4. To run application type:

		$ ruby main.rb *args
		
		where *args in this case is the multidimensional array, the triangle data.

5. Example

		$ ruby main.rb [[6],[3,5],[9,7,1],[4,6,8,4]]
		
	After type enter it should return the result to you

6. To run the tests you should run:

    $ rake spec