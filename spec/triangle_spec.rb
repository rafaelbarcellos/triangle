require_relative '../triangle'

describe Triangle do

  describe '#max_sum' do
    it 'should return 26 when the triangle is [[6],[3,5],[9,7,1],[4,6,8,4]]' do
      @triangle = Triangle.new("[[6],[3,5],[9,7,1],[4,6,8,4]]")
      expect(@triangle.max_sum).to be 26
    end
  end

end