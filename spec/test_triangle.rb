require 'test/unit'
require_relative '../triangle'

class TestSimpleNumber < Test::Unit::TestCase

  def test_sum
    @triangle = Triangle.new("[[6],[3,5],[9,7,1],[4,6,8,4]]")
    assert_equal(26, @triangle.max_sum)
  end

end
