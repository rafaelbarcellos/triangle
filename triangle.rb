
class Triangle
  require 'json'
  attr_accessor :triangle, :copy_triangle

  def initialize(triangle)
    @triangle = JSON.parse(triangle)
    @copy_triangle = JSON.parse(triangle)
  end

  def max_sum
    summed_triangle = self.copy_triangle

    for line in 0...self.triangle.size-1 do
      next_line = line+1
      for index in 0..self.triangle[line].size-1 do
        left_son = index
        right_son = index+1
        summed_triangle[next_line][left_son] = summed_triangle[line][left_son] + self.triangle[next_line][left_son] if summed_triangle[next_line][left_son] < summed_triangle[line][left_son] + self.triangle[next_line][left_son]
        summed_triangle[next_line][right_son] = summed_triangle[line][left_son] + self.triangle[next_line][right_son] if summed_triangle[next_line][right_son] < summed_triangle[line][left_son] + self.triangle[next_line][right_son]
      end
    end

    summed_triangle.last.max
  end
end